package revisao;

public class InversorDeInt {

    /**
     * Exercício 3.
     * Escreva um método chamado inverte, que receba como parâmetro um número e
     * retorne tal número em ordem invertida (de trás para a frente) sob a forma
     * de um int.
     */
    public static int inverte(int valor) {
        int invertido = 0;
        int sinal = (valor > 0) ? 1 : -1;
        valor = Math.abs(valor);
        
        while (valor > 0) {
            invertido = invertido * 10 + valor % 10;
            valor = valor / 10;
        }
        
        return invertido * sinal;
    }

    
    public static void main(String[] args) {
        System.out.println("Funcionou 12? " + (inverte(12) == 21));
        System.out.println("Funcionou 123? " + (inverte(123) == 321));
        System.out.println("Funcionou 1234567? " + (inverte(1234567) == 7654321));        
        System.out.println("Funcionou 2? " + (inverte(2) == 2));
        System.out.println("Funcionou 0? " + (inverte(0) == 0));
        System.out.println("Funcionou -1? " + (inverte(-1) == -1));
    }
}
